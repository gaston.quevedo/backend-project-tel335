const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// models
const db = require("./app/models");
const Role = db.role;
const GYM = db.gimnasio;
const Sala = db.sala;
const Horario = db.horario;

var dias_lista = ["Lunes","Martes", "Miércoles","Jueves", "Viernes","Sábado"]
var bloques_lista = ["1-2","3-4","5-6","7-8","9-10","11-12","13-14","15-16","17-18"] 
var cupos = 15;

db.sequelize.sync({force: true}).then(() => {
    console.log('Drop and Resync Db');
    initial();
    setGym();
    setSalas();
    for (var d in dias_lista) {
        for (var b in bloques_lista) {
            setHorario(dias_lista[d],bloques_lista[b], cupos, 1);
        }
    }
    for (var d in dias_lista) {
        for (var b in bloques_lista) {
            setHorario(dias_lista[d],bloques_lista[b], cupos, 2);
        }
    }
});

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to the backend." });
});
// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/reserva.routes')(app);
require('./app/routes/horario.routes')(app);
// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});

function initial() {
    Role.create({
        id: 1,
        name: "user"
    });
    Role.create({
        id: 2,
        name: "moderator"
    });
    Role.create({
        id: 3,
        name: "admin"
    });
}

function setGym(){
    GYM.create({
        id: 1,
        nombre: "gimnasio 3",
        ubicacion: "Al costado del Edificio M, casa central",
        contacto: "IG: gimnasio3usm"
    })
}

function setSalas(){
    Sala.create({
        id:1,
        nombre: "Sala multiuso",
        gimnasioId: 1
    });
    Sala.create({
        id:2,
        nombre: "Sala de musculación",
        gimnasioId: 1
    })
}

function setHorario(dia, bloque,cupos, id_sala){
    Horario.create({
        bloque: bloque,
        dia: dia,
        disponibilidad: cupos,
        salaId: id_sala
    })
}