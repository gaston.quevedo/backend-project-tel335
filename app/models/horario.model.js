module.exports = (sequelize, Sequelize) => {
    const Horario = sequelize.define("horarios", {
        bloque: {
            type: Sequelize.STRING
        },
        dia: {
            type: Sequelize.STRING
        },
        disponibilidad: {
            type: Sequelize.INTEGER
        }
    });
    return Horario;
};
