module.exports = (sequelize, Sequelize) => {
    const Sala = sequelize.define("salas", {
        nombre: {
            type: Sequelize.STRING
        }
    });
    return Sala;
};
