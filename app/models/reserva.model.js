const db = require('../models'); 
const Horario = db.horario;
const User = db.user;

module.exports = (sequelize, Sequelize) => {
    const Reserva = sequelize.define("reservas", {
        horarioId: {
            type: Sequelize.INTEGER,
            references: {
                model: Horario, 
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: User, 
                key: 'id'
            }
        }
    });
    return Reserva;
};