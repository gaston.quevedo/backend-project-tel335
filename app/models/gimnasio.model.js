module.exports = (sequelize, Sequelize) => {
    const Gimnasio= sequelize.define("gimnasios", {
        nombre: {
            type: Sequelize.STRING
        },
        ubicacion: {
            type: Sequelize.STRING
        },
        contacto: {
            type: Sequelize.STRING   
        }
    });
    return Gimnasio;
};
