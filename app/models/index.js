const config = require("../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.refreshToken = require("../models/refreshToken.model.js")(sequelize, Sequelize);
db.horario = require("../models/horario.model.js")(sequelize,Sequelize);
db.sala = require("../models/sala.model.js")(sequelize, Sequelize);
db.gimnasio = require("../models/gimnasio.model.js")(sequelize, Sequelize);
db.reserva = require("../models/reserva.model")(sequelize,Sequelize);
//Relación user-role
db.role.belongsToMany(db.user, {
    through: "user_roles",  //nombre nueva table
    foreignKey: "roleId",
    otherKey: "userId"
});
db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});

//Relación user-token
db.user.hasOne(db.refreshToken, {
    foreignKey: 'userId', targetKey: 'id'
});
db.refreshToken.belongsTo(db.user, {
    foreignKey: 'userId', targetKey: 'id'
});

//Relacion horario-usuario -> Tabla reservas
db.horario.belongsToMany(db.user, {
    through: "reservas",  //nombre nueva table
    foreignKey: "horarioId",
    otherKey: "userId"
});
db.user.belongsToMany(db.horario, {
    through: "reservas",
    foreignKey: "userId",
    otherKey: "horarioId"
});

//Relación sala-horario
db.sala.hasOne(db.horario, {
    foreignKey: 'salaId', targetKey: 'id'
});
db.horario.belongsTo(db.sala, {
    foreignKey: 'salaId', targetKey: 'id'
});

//Relación sala-gimnasio
db.gimnasio.hasMany(db.sala, {
    foreignKey: 'gimnasioId', targetKey: 'id'
});
db.sala.belongsTo(db.gimnasio, {
    foreignKey: 'gimnasioId', targetKey: 'id'
});

db.ROLES = ["user", "admin", "moderator"];
module.exports = db;