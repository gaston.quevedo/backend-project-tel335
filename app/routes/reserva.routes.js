const { authJwt } = require("../middleware");
const controller = require("../controllers/reserva.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    app.post('/api/reserva/create',
        controller.addReserva);
    app.delete('/api/reserva/delete',
        controller.deleteReserva);
    app.delete(
        "/api/reserva/deleteAllByBloque",
        [authJwt.verifyToken, authJwt.isModerator],
        controller.deleteAllReservasByBloque
    );
};
