const { authJwt } = require("../middleware");
const controller = require("../controllers/horario.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    app.put('/api/horario/disponibilidad/decrement',
        controller.decrementDisponibilidad);
    app.put('/api/horario/disponibilidad/increment',
        controller.incrementDisponibilidad);
    app.get('/api/horario/disponibilidad',
        controller.getDisponibilidad);
    app.put(
        "/api/horario/disponibilidad",
        [authJwt.verifyToken, authJwt.isModerator],
        controller.setDisponibilidad
    );
    app.get('/api/horario/id', controller.getIdHorario);
};