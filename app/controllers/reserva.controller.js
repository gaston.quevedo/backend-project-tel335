const db = require("../models");
const {reserva: Reserva} = db;

exports.addReserva = (req, res) => {
    Reserva.create({
        horarioId: req.body.horarioId,
        userId: req.body.userId
    }).then(reserva => {
        res.send({ message: `Reserva with userId: ${reserva.userId} and horarioId: ${reserva.horarioId} was created successfully!` });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.deleteReserva = (req, res) => {
    Reserva.destroy({
        where:{
            horarioId: req.body.horarioId,
            userId: req.body.userId
        }
    }).then(reserva => {
            res.send({ message: `Reserva was destroyed successfully!` });
        
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.deleteAllReservasByBloque = (req, res) => {
    Reserva.destroy({
        where:{
            horarioId: req.body.horarioId
        }
    }).then(reserva => {
        res.send({ message: `All the Reservas in this daytime were destroyed successfully!` });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}