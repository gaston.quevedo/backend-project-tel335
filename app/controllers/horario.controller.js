const db = require("../models");
const {horario: Horario} = db;

exports.decrementDisponibilidad = (req, res) => {
    Horario.findOne({ 
        where: {
            dia: req.body.dia,
            bloque: req.body.bloque
        } 
    }).then(horario => {
        if (horario.disponibilidad != 0 ){
            return horario.decrement('disponibilidad');
        }
    }).then(horario => {
        return horario.reload();
    }).then(horario => {
        res.send({ message: `Se ha decrementado la disponibilidad en 1 del horario con id: ${horario.id}` });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.incrementDisponibilidad = (req, res) => {
    Horario.findOne({ 
        where: {
            dia: req.body.dia,
            bloque: req.body.bloque
        } 
    }).then(horario => {
        return horario.increment('disponibilidad');
    }).then(horario => {
        return horario.reload();
    }).then(horario => {
        res.send({ message: `Se ha incrimentado la disponibilidad en 1 del horario con id: ${horario.id}` });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.getDisponibilidad = (req, res) =>{
    Horario.findByPk(req.body.id).then( horario => res.send({ message: horario.disponibilidad }) 
    ).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.setDisponibilidad = (req,res) =>{
    Horario.upsert({
        id: req.body.id,
        disponibilidad: req.body.disponibilidad,
    }).then( horario => {
        res.status(200).send({message: `La disponibilidad del bloque con id: ${horario.id} se actualizó correctamente`});
    }).catch(err => {res.status(500).send({ message: err.message })});
}

exports.getIdHorario = (req, res) =>{
    Horario.findOne({ 
        where: {
            dia: req.body.dia,
            bloque: req.body.bloque
        } 
    }).then( horario => res.send({ message: horario.id }) 
    ).catch(err => {
        res.status(500).send({ message: err.message });
    });
}
